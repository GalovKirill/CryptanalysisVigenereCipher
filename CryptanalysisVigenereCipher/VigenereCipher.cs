﻿using System;
using System.Text;

namespace CryptanalysisVigenereCipher
{
    public class VigenereCipher
    {
        public const string DefoultAlphabet = "abcdefghijklmnopqrstuvwxyz";
        private readonly char[,] table;
        public string Alphabet { get; }
        public string Key { get; }
        
        private char[,] InitTable()
        {
            char[,] result = new char[Alphabet.Length, Alphabet.Length];

            for (int i = 0; i < Alphabet.Length; i++)
            {
                for (int j = 0; j < Alphabet.Length; j++)
                {
                    result[i, j] = Alphabet[(i + j) % Alphabet.Length];
                    Console.Write(result[i, j]);
                }
                Console.WriteLine();
            }
            return result;

        }

        public VigenereCipher(string key, string alphabet = DefoultAlphabet)
        {        
            Key = key;
            if (alphabet != null)        
                Alphabet = alphabet;      
            table = InitTable();
        }

        public string Enrypte(string text)
        {
            StringBuilder sb = new StringBuilder(text.Length);

            for (int i = 0; i < text.Length; i++)
            {
                int indexRow = Alphabet.IndexOf(Key[i % Key.Length]);
                int indexColumn = Alphabet.IndexOf(text[i]);
                Console.WriteLine(Key[i % Key.Length] + " " + text[i]);
                sb.Append(table[indexRow, indexColumn]);
            }
            return sb.ToString();
        }
    }
}